#!/bin/bash

function fail {
    printf '%s\n' "$1" >&2
    exit "${2-1}"
}

PARAMS=""
while (("$#")); do
  case "$1" in
    -r | --region)
      if [ -n "$2" ] && [ ${2:0:1} != "-" ]; then
        region=$2
        shift 2
      else
        fail "Error Argument missing for $1!"
      fi
      ;;
    -e | --env | --environment)
      if [ -n "$2" ] && [ ${2:0:1} != "-" ]; then
        environment=$2
        shift 2
      else
        fail "Error Argument missing for $1!"
      fi
      ;;
    -p | --profile)
      if [ -n "$2" ] && [ ${2:0:1} != "-" ]; then
        profile=$2
        shift 2
      else
        fail "Error argument missing for $1!"
      fi
      ;;
    *)
      PARAMS="$PARAMS $1"
      shift
      ;;
  esac
done
eval set -- "$PARAMS"

region=${region:-$AWS_DEFAULT_REGION}
profile=${profile:-synaptech}

# verify parameters

if [[ -z "$environment" ]]; then
  fail "Error missing --environment parameter or environment config option!"
fi
if [[ -z "$region" ]]; then
  fail "Error missing --region parameter or region config option!"
fi

aws s3 sync build/ s3://com.swypeless.$region.$environment.terminal
