# Swypeless Terminal App

## Environment Variables

| Variable                         | Required | Type    | Example                            | Notes                                            |
|----------------------------------|----------|---------|------------------------------------|--------------------------------------------------|
| REACT_APP_API_BASE_URL           | Yes      | url     | http://terminal.localhost:3000/api |                                                  |
| REACT_APP_API_MAX_RETRIES        | No       | integer | 3                                  |                                                  |
| REACT_APP_API_TIMEOUT            | No       | integer | 15000                              | Number of milliseconds of api connection timeout |
| REACT_APP_STRIPE_PUBLISHABLE_KEY | Yes      | string  | pk_test_3215sfsdfj4390...          |                                                  |
| REACT_APP_GA_MEASUREMENT_ID      | Yes*     | string  | G-0000000000                       | GA4 Measurement ID (Not required in local environment) |

## Adding Variables to your Environment

There are three methods to add variables to the environment where your app is running.

**1. Manually Export the Variable to your Environment**

Enter into your terminal, replacing the variable name and value:

```
export VARIABLE=value
```

This method is not recommended because the terminal does not save your variables accross sessions.

**2. Export the Variable to your Environment using `direnv`**

Use `direnv` ([direnv](https://direnv.net/)) to manage environment variables in `.envrc` files that are ignored by `.gitignore`.

This method is suggested for static variables and variables that will be shared accross projects.

**3. Use a `.env` file locally**

This service loads environment variables from `.env` files from three filename formats:

- `.env`
- `.env.SERVICE_ENV--[environment]`
- `.env.NODE_ENV--[environment]`

Replace '`[environment]`' with the service and node environments respectively.

This can be used to separate environment specific variable values.

*Service environments:*

- `local`
- `stage`
- `prod`

## Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
