import axios, { AxiosInstance, AxiosRequestConfig, AxiosResponse } from 'axios';
import { StatusCodes } from 'http-status-codes';
import { API_BASE_URL, API_MAX_RETRIES, API_TIMEOUT, API_TOKEN_HEADER } from 'config/constants';
import { ApiRequestOptions } from './api-request-options';
import {
  ApiException,
  BadRequestApiException,
  ConflictApiException,
  ConnectionApiException,
  ForbiddenApiException,
  ServerFailuerApiException,
  UnauthorizedApiException,
  UnknownApiException,
} from './exceptions';
import { SalesApiModule } from './modules';
import { buildBearerAuthHeader, paramsSerializer } from './utils';

export const API_VERSION = '2021-01-01';

export interface  ApiClientParams {
  baseURL: string;
  version: string;
  maxRetries?: number;
  timeout?: number;
}

type RequestConfig = AxiosRequestConfig & Omit<ApiRequestOptions, 'path'>;

type RequestInterceptor = (request: AxiosRequestConfig) => void;
type ResponseInterceptor = (response: AxiosResponse) => void;
type ResponseExceptionInterceptor = (
  request: AxiosRequestConfig,
  exception: ApiException,
  willRetry: boolean,
) => void;

export class ApiClient {
  private client: AxiosInstance;
  private readonly maxRetries: number;

  private requestInterceptors: RequestInterceptor[] = [];
  private responseInterceptors: ResponseInterceptor[] = [];
  private responseExceptionInterceptors: ResponseExceptionInterceptor[] = [];

  public readonly sales: SalesApiModule;

  constructor({
    baseURL,
    version,
    maxRetries,
    timeout,
  }: ApiClientParams) {
    this.maxRetries = maxRetries || 1;

    // create axios client
    this.client = axios.create({
      baseURL,
      timeout,
      paramsSerializer,
      headers: {
        'Swypeless-Version': version,
      },
    });

    // setup interceptors
    this.client.interceptors.request.use(this.requestInterceptor);
    this.client.interceptors.response.use(
      this.responseInterceptor,
      this.responseErrorInterceptor,
    );

    // initialize modules
    this.sales = new SalesApiModule(this.client);
  }

  public setAccessToken(token: string): void {
    this.client.defaults.headers.common.Authorization = buildBearerAuthHeader(token);
  }

  public registerRequestInterceptor(interceptor: RequestInterceptor) {
    this.requestInterceptors.push(interceptor);
  }

  public registerResponseInterceptor(interceptor: ResponseInterceptor) {
    this.responseInterceptors.push(interceptor);
  }

  public registerResponseExceptionInterceptor(interceptor: ResponseExceptionInterceptor) {
    this.responseExceptionInterceptors.push(interceptor);
  }

  private requestInterceptor = (request: AxiosRequestConfig): AxiosRequestConfig => {
    // call request interceptors
    this.requestInterceptors.forEach((interceptor) => interceptor(request));
    // return request
    return request;
  }

  private responseInterceptor = async (response: AxiosResponse): Promise<AxiosResponse> => {
    // call response interceptors
    this.responseInterceptors.forEach((interceptor) => interceptor(response));
    // return response
    return response;
  }

  private responseErrorInterceptor = async (error: any): Promise<any> => {
    const config = Object.assign({}, error.config) as RequestConfig;
    let exception: ApiException;
    let maybeRetry = false;
    if (error.response) {
      const response = error.response as AxiosResponse<any>;
      switch (response.status) {
        case StatusCodes.BAD_REQUEST:
          exception = new BadRequestApiException();
          break;
        case StatusCodes.UNAUTHORIZED:
          exception = new UnauthorizedApiException();
          break;
        case StatusCodes.FORBIDDEN:
          exception = new ForbiddenApiException();
          break;
        case StatusCodes.CONFLICT:
          exception = new ConflictApiException();
          break;
        case StatusCodes.INTERNAL_SERVER_ERROR:
        case StatusCodes.BAD_GATEWAY:
        case StatusCodes.GATEWAY_TIMEOUT:
          exception = new ServerFailuerApiException();
          maybeRetry = true;
          break;
        default:
          exception = new UnknownApiException();
          break;
      }
    } else if (error.request) {
      exception = new ConnectionApiException();
      maybeRetry = true;
    } else {
      exception = new UnknownApiException();
    }
    // retry flag
    const retry = maybeRetry
      && !config.preventRetry
      && (config.retries || 0) < this.maxRetries;
    // call response exception interceptors
    this.responseExceptionInterceptors.forEach((interceptor) => interceptor(config, exception, retry));
    // maybe retry request
    if (retry) {
      // increment retries
      config.retries = (config.retries || 0) + 1;
      // retry
      return this.client(config);
    }
    // throw exception
    throw exception;
  }
}

export const apiClient = new ApiClient({
  baseURL: API_BASE_URL,
  version: API_VERSION,
  maxRetries: API_MAX_RETRIES,
  timeout: API_TIMEOUT,
});

apiClient.registerResponseInterceptor((response) => {
  const token = response.headers[API_TOKEN_HEADER];
  if (token) {
    apiClient.setAccessToken(token);
  }
})
