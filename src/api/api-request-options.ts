import { AxiosRequestConfig, Method } from 'axios';

export interface ApiRequestOptions extends Pick<AxiosRequestConfig,
  'params' |
  'data' |
  'headers'
> {
  method: Method;
  path?: any | any[];
  preventRetry?: boolean;
  retries?: number;
}