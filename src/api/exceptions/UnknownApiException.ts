import { ApiException } from './ApiException.interface';

export class UnknownApiException extends ApiException {
  constructor() {
    super('Unknown');
  }
}
