import { ApiException } from './ApiException.interface';

export class NotFoundApiException extends  ApiException {
  constructor() {
    super('Not Found');
  }
}
