import { ApiException } from './ApiException.interface';

export class UnauthorizedApiException extends ApiException {
  constructor() {
    super('Unauthorized');
  }
}
