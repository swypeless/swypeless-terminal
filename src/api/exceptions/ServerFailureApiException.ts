import { ApiException } from './ApiException.interface';

export class ServerFailuerApiException extends ApiException {
  constructor() {
    super('Server Failure');
  }
}
