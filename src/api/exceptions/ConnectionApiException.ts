import { ApiException } from './ApiException.interface';

export class ConnectionApiException extends ApiException {
  constructor() {
    super('Connection');
  }
}
