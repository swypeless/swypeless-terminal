import { ApiException } from './ApiException.interface';

export class ConflictApiException extends ApiException {
  constructor() {
    super('Conflict');
  }
}
