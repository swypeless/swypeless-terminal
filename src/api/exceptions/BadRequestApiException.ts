import { ApiException } from './ApiException.interface';

export class BadRequestApiException extends ApiException {
  constructor() {
    super('Bad Request');
  }
}
