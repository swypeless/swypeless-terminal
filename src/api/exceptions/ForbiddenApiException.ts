import { ApiException } from './ApiException.interface';

export class ForbiddenApiException extends ApiException {
  constructor() {
    super('Forbidden');
  }
}
