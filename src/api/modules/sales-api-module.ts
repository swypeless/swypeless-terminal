import { AxiosInstance } from 'axios';
import { Sale } from 'types';
import { ApiModule } from './api-module.interface';

export interface SaleUpdateTipOptions {
  amount: number | null;
}

export interface SaleUpdateReceiptEmailOptions {
  receipt_email: string | null;
}

export class SalesApiModule extends ApiModule {
  constructor(client: AxiosInstance) {
    super(client, 'sales');
  }

  async retrieve(id: string): Promise<Sale> {
    return this.request({
      method: 'GET',
      path: id,
    });
  }

  async updateTip(id: string, options: SaleUpdateTipOptions): Promise<Sale> {
    return this.request({
      method: 'PUT',
      path: `${id}/tip`,
      data: options,
    });
  }

  async updateReceiptEmail(id: string, options: SaleUpdateReceiptEmailOptions): Promise<Sale> {
    return this.request({
      method: 'PUT',
      path: `${id}/receipt`,
      data: options,
    });
  }
}