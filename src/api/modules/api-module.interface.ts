import { AxiosInstance } from 'axios';
import { ApiRequestOptions } from '../api-request-options';

export abstract class ApiModule {
  constructor(
    private readonly client: AxiosInstance,
    protected readonly basePath?: string,
  ) {}

  protected async request<T>(options: ApiRequestOptions): Promise<T> {
    const { path, ...requestOptions } = options;
    const response = await this.client.request<T>({
      ...requestOptions,
      url: this.buildPath(path),
    });
    return response.data;
  }

  protected buildPath(path?: string | string[]): string {
    const pathParts = path ? (Array.isArray(path) ? path : [path]) : [];
    if (this.basePath) {
      pathParts.unshift(this.basePath);
    }
    return pathParts.join('/');
  }
}
