import React, { useCallback, useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import {
  SaleStatus,
} from 'types';
import { PendingSale } from 'components/Checkout/PendingSale/PendingSale';
import { CompletedSale } from 'components/Checkout/CompletedSale/CompletedSale';
import { Loader } from 'components/Loader/Loader';
import { useCheckoutStore } from 'stores';
import { CheckoutError } from 'components/Checkout/CheckoutError/CheckoutError';

export const CheckoutPage = () => {
  const { saleId } = useParams();
  const {
    loaded,
    sale,
    loadSale,
    paymentError,
    setPaymentError,
    sendReceipt,
  } = useCheckoutStore();
  const [canSendReceipt, setCanSendReceipt] = useState<boolean | null>(null);

  const tryLoadSale = useCallback(() => {
    if (saleId) {
      loadSale(saleId)
    } else {
      // ERROR: should never happen, ignore
    }
  }, [loadSale, saleId]);

  const paymentErrorTryAgain = useCallback(() => {
    setPaymentError(null);
  }, [setPaymentError]);

  useEffect(() => {
    tryLoadSale();
  }, [tryLoadSale]);

  useEffect(() => {
    if (canSendReceipt === null && sale !== null) {
      // can only set receipt if original sale status is in uncompleted state
      setCanSendReceipt([
        SaleStatus.Pending,
        SaleStatus.Processing,
        SaleStatus.RequiresAction,
      ].includes(sale.status));
    }
  }, [sale, canSendReceipt, setCanSendReceipt]);

  if (paymentError) {
    return <CheckoutError heading="Payment Error" message={paymentError} onAction={paymentErrorTryAgain} />
  }

  if (sale) {
    switch (sale.status) {
      case SaleStatus.Pending:
      case SaleStatus.Processing:
      case SaleStatus.RequiresAction:
        return <PendingSale />;
      case SaleStatus.Completed:
      case SaleStatus.PartiallyRefunded:
      case SaleStatus.Refunded:
        return <CompletedSale sale={sale} sendReceipt={canSendReceipt ? sendReceipt : undefined} />;
      case SaleStatus.Canceled:
        return <CheckoutError heading="Sale Canceled" />
      default:
        break;
    }
  }

  if (loaded) {
    return <CheckoutError heading="Something went wrong" message="Please try again" onAction={tryLoadSale} />
  }

  return <Loader size={65} />;
}