import React from 'react';
import { Typography } from '@mui/material';
import { Box } from '@mui/system';

export const NotFoundPage = () => {
  return (
      <Box mt={10} p={5} pt={10}>
        <Typography variant="h1">404</Typography>
        <Typography>Something's wrong</Typography>
      </Box>
  );
}