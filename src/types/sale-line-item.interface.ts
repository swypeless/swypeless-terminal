export interface SaleLineItem {
  label: string;
  amount: number | null;
  currency: string;
}
