export function addIdPrefix(id: string, prefix: string): string {
  if (id.includes('_')) {
    return id;
  }
  return `${prefix}_${id}`;
}
