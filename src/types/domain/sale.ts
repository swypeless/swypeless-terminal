import { Merchant } from './merchant';
import { DomainObject, Identifiable, Mutable } from './common';

export const SALE_PREFIX = 'sale';

export enum ConvenienceFeeMode {
  Automatic = 'automatic',
  Custom = 'custom',
}

export enum TipMode {
  Smart = 'smart',
  Custom = 'custom',
}

export interface TipOption {
  amount: number; // mcus
  percent: number | null; // pips
}

export enum SaleType {
  Simple = 'simple',
}

export enum SaleStatus {
  Pending = 'pending',
  Processing = 'processing',
  RequiresAction = 'requires_action',
  Canceled = 'canceled',
  Completed = 'completed',
  PartiallyRefunded = 'partially_refunded',
  Refunded = 'refunded',
}

export enum SalePaymentType {
  Direct = 'direct',
  Destination = 'destination',
}

export interface Sale extends DomainObject, Identifiable, Mutable {
  type: SaleType;
  currency: string;
  amount: number; // mcus
  subtotal: number; // mcus
  receipt_email: string | null;
  customer_note: string | null;
  status: SaleStatus;
  stripe_account_id: string;
  stripe_payment_intent_id: string;
  stripe_client_secret: string | null;
  payment_type: SalePaymentType;
  products: SaleProduct[];
  components: SaleComponent[];
  merchant: Merchant;
}
export enum SalePriceType {
  Simple = 'simple',
}

export interface SalePrice {
  type: SalePriceType;
}

export interface SimpleSalePrice extends SalePrice {
  unit_amount: number; // mcus
}

export enum SaleProductType {
  Standard = 'standard',
  Custom = 'custom',
}

export interface SaleProduct extends DomainObject, Identifiable, Mutable {
  sale_id: string;
  type: SaleProductType;
  parent: string | null;
  name: string | null;
  description: string | null;
  currency: string;
  quantity: number;
  amount: number; // mcus
  price: SalePrice;
}

export enum SaleComponentType {
  Tip = 'tip',
  Tax = 'tax',
  ConvenienceFee = 'convenience_fee',
}

export interface SaleComponent extends DomainObject, Identifiable, Mutable {
  sale_id: string;
  type: SaleComponentType;
  currency: string;
  amount: number | null; // mcus
}

export interface ConvenienceFeeSaleComponent extends SaleComponent {
  mode: ConvenienceFeeMode;
  rate: number | null; // pips
  base: number | null; // mcus
}

export interface TaxSaleComponent extends SaleComponent {
  rate: number; // pips
}

export interface TipSaleComponent extends SaleComponent {
  mode: TipMode;
  options: TipOption[];
}
