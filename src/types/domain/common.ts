export interface DomainObject {
  object: string;
}

export interface Identifiable<KeyType = string> {
  id: KeyType;
}

export interface Creatable {
  created: number;
}

export interface Mutable extends Creatable {
  modified: number;
}
