export interface Merchant {
  country: string;
  display_name: string | null;
  branding: Merchant.Branding;
}

export declare namespace Merchant {
  export interface Branding {
    icon: string | null;
    logo: string | null;
    primary_color: string | null;
    secondary_color: string | null;
  }
}