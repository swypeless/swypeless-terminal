import React from 'react';
import { Container, ThemeProvider } from '@mui/material';
import { theme } from './styles/theme';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { NotFoundPage } from './pages/NotFoundPage/NotFoundPage';
import { CheckoutPage } from './pages/CheckoutPage/CheckoutPage';
import { PageTracker } from './components/Tracking/PageTracker';
import Analytics from './analytics';

Analytics.initialize();

function App() {
  return (
    <ThemeProvider theme={theme}>
      <Container maxWidth="sm" disableGutters>
        <BrowserRouter>
          <Routes>
            <Route path="checkout/:saleId" element={<CheckoutPage />} />
            <Route path="*" element={<NotFoundPage />} />
          </Routes>
          <PageTracker />
        </BrowserRouter>
      </Container>
    </ThemeProvider>
  );
}

export default App;
