import { useEffect } from 'react';
import { useLocation } from 'react-router-dom';
import Analytics from '../../analytics';

export const PageTracker = () => {
  const location = useLocation();

  useEffect(() => {
    Analytics.trackPage(location.pathname);
  }, [location]);

  return null;
};

export default PageTracker;
