import React from 'react';
import { Box, Button, Stack, Typography } from '@mui/material';
import EastRoundedIcon from '@mui/icons-material/EastRounded';
import logo from 'assets/logo.svg';
import { Merchant } from 'types';
import { CurrencyUtils } from 'utils/currency-utils';
import { useFitDimension } from 'hooks/useFitDimension';

export interface SaleSummaryProps {
  merchant: Merchant;
  amount: number,
  currency: string,
  onViewDetailsClick: () => void;
}

export const SaleSummary = ({
  merchant,
  amount,
  currency,
  onViewDetailsClick,
}: SaleSummaryProps) => {
  const { dimension, ref } = useFitDimension({
    initial: 80,
    min: 40,
  });
  
  return (
    <Stack
      direction="column"
      spacing={5}
      justifyContent="center"
      alignItems="center"
      sx={{ width: '100%', textAlign: 'center' }}
    >
      <Box>
        <img src={logo} alt="logo" height={60} />
      </Box>
      <Stack spacing={2} sx={{ width: '100%' }}>
        <Typography variant="body2" color="text.secondary" fontSize="0.7rem" fontWeight="bold" sx={{ textTransform: "uppercase" }}>
          Amount due{merchant.display_name && ` to ${merchant.display_name}`}:
        </Typography>
        <Typography variant="h1" color="text.primary" fontSize={`${dimension}px`} ref={ref} sx={{ width: '100%' }}>
          {CurrencyUtils.formatCurrency(amount, currency)}
        </Typography>
      </Stack>
      <Button onClick={onViewDetailsClick} sx={{ color: 'text.secondary', textTransform: 'none' }}>
        View Details
        <EastRoundedIcon fontSize="small" sx={{ marginLeft: 1 }} />
      </Button>
    </Stack>
  );
};