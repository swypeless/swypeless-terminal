import React, { useCallback, useState } from 'react';
import Container from '@mui/material/Container';
import Stack from '@mui/material/Stack';
import Divider from '@mui/material/Divider';
import { SaleComponentType, TipSaleComponent } from 'types';
import { SaleDetails } from '../SaleDetails/SaleDetails';
import { SaleSummary } from '../SaleSummary/SaleSummary';
import { TipControls } from '../TipControls/TipControls';
import { PaymentControls } from '../PaymentControls/PaymentControls';
import { STRIPE_API_VERSION, STRIPE_PUBLISHABLE_KEY } from 'config/constants';
import { loadStripe, StripeElementsOptions } from '@stripe/stripe-js';
import { Elements } from '@stripe/react-stripe-js';
import { useCheckoutStore } from 'stores';

export const PendingSale = () => {
  const {
    sale,
    amountNoTip,
    tipAmount,
    setTipAmount,
    maybeUpdateSaleTip,
  } = useCheckoutStore((state) => ({
    sale: state.sale,
    amountNoTip: state.amountNoTip,
    tipAmount: state.tipAmount,
    setTipAmount: state.setTipAmount,
    maybeUpdateSaleTip: state.maybeUpdateSaleTip,
  }));
  const [isShowingDetails, setIsShowingDetails] = useState(false);
  const [pendingTipUpdates, setPendingTipUpdates] = useState(0);
  const [stripePromise] = useState(() => loadStripe(STRIPE_PUBLISHABLE_KEY, {
    apiVersion: STRIPE_API_VERSION,
    stripeAccount: (sale && sale.stripe_account_id) || undefined,
  }));

  // sale must not be null for this component
  if (sale === null) {
    throw new Error('Sale is null');
  }

  const showDetails = useCallback(() => {
    setIsShowingDetails(true);
  }, [setIsShowingDetails]);
  const hideDetails = useCallback(() => {
    setIsShowingDetails(false);
  }, [setIsShowingDetails]);

  const incrementTipUpdates = useCallback(() => {
    setPendingTipUpdates(pendingTipUpdates + 1);
  }, [pendingTipUpdates, setPendingTipUpdates]);
  const decrementTipUpdates = useCallback(() => {
    setPendingTipUpdates(Math.max(pendingTipUpdates - 1, 0));
  }, [pendingTipUpdates, setPendingTipUpdates]);

  const updateTip = useCallback(async (tipAmount: number | null) => {
    incrementTipUpdates();
    setTipAmount(tipAmount);
    await maybeUpdateSaleTip();
    decrementTipUpdates();
  }, [setTipAmount, maybeUpdateSaleTip, incrementTipUpdates, decrementTipUpdates])

  const ready = pendingTipUpdates === 0;

  const stripeOptions: StripeElementsOptions = {
    clientSecret: sale.stripe_client_secret || '',
    appearance: {
      variables: {
        fontSizeBase: '16px',
      },
    },
  };

  const tipComponent = sale.components
    .find((component) => component.type === SaleComponentType.Tip) as TipSaleComponent | undefined;

  return (
    <Stack
      height="100%"
      divider={<Divider flexItem />}
    >
      {isShowingDetails
        ? <SaleDetails onBackClick={hideDetails} sale={sale} />
        : <Container sx={{ height: '100%' }}>
          <Stack
            justifyContent="space-evenly"
            alignItems="center"
            spacing={4}
            height="100%"
            sx={{ overflowY: "auto" }}
          >
            <SaleSummary
              amount={amountNoTip}
              currency={sale.currency}
              merchant={sale.merchant}
              onViewDetailsClick={showDetails}
            />
            {tipComponent && <TipControls
              options={tipComponent.options}
              currency={tipComponent.currency}
              amount={tipAmount}
              setAmount={updateTip}
              saleAmountNoTip={amountNoTip}
            />}
          </Stack>
        </Container>}
      <Container>
        <Elements stripe={stripePromise} options={stripeOptions}>
          <PaymentControls sale={sale} ready={ready} />
        </Elements>
      </Container>
    </Stack>
  );
}