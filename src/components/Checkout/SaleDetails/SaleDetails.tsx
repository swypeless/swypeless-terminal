import React from 'react';
import { 
  Stack,
  Divider,
  Container,
  Button,
  Box,
  styled,
  Typography,
  SvgIcon,
} from '@mui/material';
import WestRoundedIcon from '@mui/icons-material/WestRounded';
import { Sale } from 'types';
import { SaleDetailsLineItem } from './SaleDetailsLineItem';
import { SaleDetailsProduct } from './SaleDetailsProduct';
import { SaleDetailsBox } from './SaleDetailsBox';
import { ReactComponent as CustomerNoteIcon } from 'assets/CustomerNoteIcon.svg';
import { SaleUtils } from 'utils/sale-utils';

export interface SaleDetailsProps {
  sale: Sale;
  onBackClick: () => void;
}

const SaleDetailsLineItemsStack = styled(Stack)(({ theme }) => ({
  '& .MuiGrid-container': {
    '&:last-of-type *': {
      fontWeight: 'bold',
      color: theme.palette.text.primary,
    },
  },
}));

export const SaleDetails = ({
  sale,
  onBackClick,
}: SaleDetailsProps) => {
  const lineItems = [
    ...SaleUtils.getSaleLineItems(sale),
    {
      label: 'Total',
      amount: sale.amount,
      currency: sale.currency,
    },
  ];

  // map products
  const multipleCustomProducts = sale.products.reduce((prev, product) => product.name === null ? prev + 1 : prev, 0) > 1;
  let customProductIndex = 0;
  const products = sale.products.map((product, index) => ({
    name: product.name || (multipleCustomProducts
      ? `Custom Product ${String.fromCharCode(65 + customProductIndex++)}`
      : `Custom Product`),
    quantity: product.quantity,
    amount: product.amount,
    currency: product.currency,
    key: index,
  }));

  return (
    <Stack
      height="100%"
      divider={<Divider flexItem />}
      className="Swplss--flex-scrollable-container"
    >
      <Stack
        height="100%"
        className="Swplss--flex-scrollable-container"
      >
        <Box>
          <Button onClick={onBackClick} sx={{ color: 'text.secondary', textTransform: 'none', padding: 2 }}>
            <WestRoundedIcon fontSize="small" sx={{ marginRight: 1 }} />
            Back
          </Button>
        </Box>
        <Box flexGrow={1} className="Swplss--flex-scrollable-container">
          <Container className="Swplss--flex-scrollable-container" sx={{ overflowY: "auto", pt: 2 }}>
            <Typography variant="body2" color="selected.main" fontSize="0.7rem" fontWeight="bold" sx={{ textTransform: "uppercase" }}>
              Items
            </Typography>
            <Stack
              marginY={3}
              spacing={2}
              divider={<Divider flexItem sx={{ borderColor: "dividerSecondary" }} />}
              width="100%"
            >
              {products.map((product) => <SaleDetailsProduct {...product} />)}
            </Stack>
            {sale.customer_note && <>
              <Typography
                variant="body2"
                color="selected.main"
                fontSize="0.7rem"
                fontWeight="bold"
                mt={4}
                sx={{ textTransform: "uppercase" }}
              >
                Notes
              </Typography>
              <Stack
                direction="row"
                alignItems="top"
                justifyContent="space-between"
                width="100%"
                spacing={3}
                my={3}
              >
                <SaleDetailsBox bgcolor="selected.main">
                  <SvgIcon component={CustomerNoteIcon} sx={{ color: "common.white" }} />
                </SaleDetailsBox>
                <Box sx={{ flexGrow: 1 }}>
                  <Typography color="text.primary">{sale.customer_note}</Typography>
                </Box>
              </Stack>
            </>}
          </Container>
        </Box>
      </Stack>
      <Container>
        <SaleDetailsLineItemsStack
          marginY={3}
          spacing={1.5}
        >
          {lineItems.map((lineItem, index) => <SaleDetailsLineItem {...lineItem} key={index} />)}
        </SaleDetailsLineItemsStack>
      </Container>
    </Stack>
  );
}
