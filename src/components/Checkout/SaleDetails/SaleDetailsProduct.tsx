import { Box, Stack, Typography } from '@mui/material';
import { CurrencyUtils } from '../../../utils/currency-utils';
import { SaleDetailsBox } from './SaleDetailsBox';

export interface SaleDetailsProductProps {
  name: string;
  quantity: number;
  amount: number;
  currency: string;
}

export const SaleDetailsProduct = ({
  name,
  quantity,
  amount,
  currency,
}: SaleDetailsProductProps) => (
  <Stack
    direction="row"
    alignItems="center"
    justifyContent="space-between"
    width="100%"
    spacing={3}
  >
    <SaleDetailsBox bgcolor="selected.main">
      <Typography color="common.white">{quantity}</Typography>
    </SaleDetailsBox>
    <Box sx={{ flexGrow: 1 }}>
        <Typography color="text.primary">{name}</Typography>
    </Box>
    <Typography color="text.secondary">{CurrencyUtils.formatCurrency(amount, currency)}</Typography>
  </Stack>
);