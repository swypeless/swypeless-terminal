import React from 'react';
import Box, { BoxProps } from '@mui/material/Box';

export interface SaleDetailsBoxProps extends BoxProps {
  size?: number;
}

export const SaleDetailsBox = ({
  size = 40,
  children,
  ...otherProps
}: SaleDetailsBoxProps) => (
  <Box>
    <Box
      width={size}
      height={size}
      borderRadius={1}
      display="flex"
      alignItems="center"
      justifyContent="center"
      {...otherProps}
    >
      {children}
    </Box>
  </Box>
)