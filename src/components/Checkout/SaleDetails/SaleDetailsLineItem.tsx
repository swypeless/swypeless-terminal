import { Grid, Typography } from '@mui/material';
import { CurrencyUtils } from 'utils/currency-utils';
import { SaleLineItem } from 'types';

export interface SaleDetailsLineItemProps extends SaleLineItem {}

export const SaleDetailsLineItem = ({
  label,
  amount,
  currency,
}: SaleDetailsLineItemProps) => (
  <Grid
    container
    direction="row"
    justifyContent="space-between"
  >
    <Grid item>
      <Typography fontSize={14} color="text.primary">{label}</Typography>
    </Grid>
    <Grid item>
      <Typography fontSize={14} color="text.secondary">{CurrencyUtils.formatCurrency(amount || 0, currency)}</Typography>
    </Grid>
  </Grid>
);
