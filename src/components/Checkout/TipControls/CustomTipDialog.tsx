import React, { ChangeEvent, useCallback, useEffect, useRef, useState } from 'react';
import Dialog from '@mui/material/Dialog'
import DialogTitle from '@mui/material/DialogTitle';
import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import { CurrencyUtils } from 'utils/currency-utils';
import ClearIcon from '@mui/icons-material/Clear';
import IconButton from '@mui/material/IconButton';
import InputAdornment from '@mui/material/InputAdornment';
import { NumericTextField } from 'components/NumericTextField/NumericTextField';
import { CURRENCY_MAX_DIGITS } from 'config/constants';
import UtilityMath from 'utils/utility-math';

export interface CustomTipDialogProps {
  open: boolean;
  amount: number | null;
  saleAmountNoTip: number;
  currency: string;
  onClose: (amount: number | null) => void;
}

export const CustomTipDialog = ({
  open,
  onClose,
  amount,
  saleAmountNoTip,
  currency,
}: CustomTipDialogProps) => {
  const currencyFractionDigits = CurrencyUtils.currencyFractionDigits(currency);
  const maxIntegerDigits = CURRENCY_MAX_DIGITS - currencyFractionDigits;
  
  const inputRef = useRef<HTMLInputElement>();

  const [inputText, setInputText] = useState<string | undefined>(amount
    ? `${UtilityMath.mcusToDecimal(amount, currencyFractionDigits)}`
    : '');
  
  const inputMcus = CurrencyUtils.parseCurrencyAsMcus(inputText, currency);

  useEffect(() => inputRef.current?.scrollIntoView(), [inputRef]);

  const handleClose = useCallback(() => {
    onClose(amount);
  }, [onClose, amount]);

  const handleContinue = useCallback(() => {
    onClose(inputMcus);
  }, [onClose, inputMcus]);

  const handleClearClick = useCallback(() => {
    setInputText('');
    inputRef.current?.focus();
  }, [setInputText, inputRef]);
  const handleChange = useCallback((event: ChangeEvent<HTMLInputElement>) => {
    setInputText(event.target.value);
  }, [setInputText]);

  return (
    <Dialog onClose={handleClose} open={open}>
      <DialogTitle sx={{ textAlign: 'center' }}>Choose custom amount</DialogTitle>
      <Stack
        alignItems="center"
        spacing={4}
        m={2}
      >
        <Stack
          alignItems="center"
          spacing={2}
        >
          <NumericTextField
            maxFractionDigits={currencyFractionDigits}
            maxIntegerDigits={maxIntegerDigits}
            value={inputText}
            onChange={handleChange}
            fullWidth={true}
            focused
            autoFocus
            size="small"
            inputRef={inputRef}
            onFocus={(event) => event.target.select()}
            inputProps={{ style: { fontSize: '32px', textAlign: 'center' }}}
            InputProps={{
              startAdornment: <InputAdornment position="start" sx={{ width: '40px' }}>
                <Typography fontSize="24px">{CurrencyUtils.currencySymbol(currency) || ''}</Typography>
              </InputAdornment>,
              endAdornment: <InputAdornment position="end">
                <IconButton
                  edge="end"
                  onClick={handleClearClick}
                >
                  <ClearIcon />
                </IconButton>
              </InputAdornment>,
            }}
          />
          <Typography variant="body2" fontSize="0.7rem" sx={{ textTransform: "uppercase" }}>
            Amount due: {CurrencyUtils.formatCurrency(saleAmountNoTip, currency)}
          </Typography>
        </Stack>
        <Button variant="contained" fullWidth={true} onClick={handleContinue}>Continue</Button>
      </Stack>
    </Dialog>
  )
}