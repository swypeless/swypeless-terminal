import {
  Stack,
  styled,
  ToggleButton,
  ToggleButtonGroup,
  Typography,
} from '@mui/material';
import React, { useCallback, useState } from 'react';
import { TipOption } from 'types';
import { CurrencyUtils } from 'utils/currency-utils';
import { PercentUtils } from 'utils/percent-utils';
import { CustomTipDialog } from './CustomTipDialog';

const StyledToggleButtonGroup = styled(ToggleButtonGroup)(({ theme }) => ({
  '& .MuiToggleButtonGroup-grouped': {
    margin: theme.spacing(2),
    color: theme.palette.text.secondary,
    borderColor: theme.palette.text.secondary,
    '&': {
      border: '1px solid',
    },
    '&:not(:first-of-type)': {
      borderRadius: theme.shape.borderRadius,
      borderLeft: '1px solid',
    },
    '&:first-of-type': {
      borderRadius: theme.shape.borderRadius,
      marginLeft: 0,
    },
    '&:last-of-type': {
      marginRight: 0,
    },
    '&.Mui-selected': {
      borderColor: theme.palette.selected.main,
      backgroundColor: theme.palette.selected.main,
      color: theme.palette.common.white,
    },
    '&.Mui-selected:hover': {
      backgroundColor: theme.palette.selected.main,
    },
  },
}));

const TipOptionButton = (
  top: string,
  bottom: string | null,
  value: number | null,
  onClick?: () => void,
  key?: any,
) => (
  <ToggleButton value={value || ''} key={key} onClick={onClick}>
    <Stack alignItems="center">
      <Typography>{top}</Typography>
      {bottom && <Typography fontSize="0.7rem">{bottom}</Typography>}
    </Stack>
  </ToggleButton>
);

export interface TipControlsParams {
  currency: string;
  options: TipOption[];
  amount: number | null;
  setAmount: (amount: number | null) => void;
  saleAmountNoTip: number;
}

export const TipControls = ({
  currency,
  options,
  amount,
  setAmount,
  saleAmountNoTip,
}: TipControlsParams) => {
  const amountMatchesOption = useCallback((amount: number) => options
    .find((option) => option.amount === amount) !== undefined,
    [options],
  );

  const [customAmount, setCustomAmount] = useState<number | null>(
    amount && amountMatchesOption(amount) ? null : amount,
  );
  const [customTipDialogOpen, setCustomTipDialogOpen] = useState(false);

  const customAmountFormatted = customAmount
    ? CurrencyUtils.formatCurrency(customAmount, currency)
    : null;

  const handleGroupChange = useCallback((
    event: React.MouseEvent<HTMLElement>,
    newAmount: any,
  ) => {
    if (newAmount === null || typeof newAmount === 'number') {
      setAmount(newAmount);
    }
  }, [setAmount]);

  const customClick = useCallback(() => {
    setCustomTipDialogOpen(true);
  }, [setCustomTipDialogOpen]);

  const onCustomDialogClose = useCallback((amount: number | null) => {
    if (amount === null || !amountMatchesOption(amount)) {
      setCustomAmount(amount);
    } else {
      setCustomAmount(null);
    }
    setAmount(amount);
    setCustomTipDialogOpen(false);
  }, [setAmount, setCustomAmount, setCustomTipDialogOpen, amountMatchesOption]);

  const tipOptionMapper = (option: TipOption, index: number) => {
    const amount = CurrencyUtils.formatCurrency(option.amount, currency);
    const percent = option.percent !== null
      ? PercentUtils.formatPercentFromPips(option.percent)
      : null;
    const top = percent ? percent : amount;
    const bottom = percent ? amount : null;
    return TipOptionButton(top, bottom, option.amount, undefined, index);
  };

  return (
    <>
      <Stack alignItems="center" width="100%">
        <Typography variant="body2" color="text.secondary" fontSize="0.7rem" fontWeight="bold" sx={{ textTransform: "uppercase" }}>
          Tipping
        </Typography>
        <StyledToggleButtonGroup exclusive value={amount} onChange={handleGroupChange} fullWidth>
          {[...options].map(tipOptionMapper)}
          {TipOptionButton("Custom", customAmountFormatted, customAmount, customClick, options.length)}
        </StyledToggleButtonGroup>
      </Stack>
      <CustomTipDialog
        open={customTipDialogOpen}
        saleAmountNoTip={saleAmountNoTip}
        currency={currency}
        amount={customAmount}
        onClose={onCustomDialogClose}
      />
    </>
  );
};
