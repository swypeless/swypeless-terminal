import React from 'react';
import Divider from '@mui/material/Divider';
import Stack from '@mui/material/Stack';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';
import { Xmark } from '../../Xmark/Xmark';
import Button from '@mui/material/Button';

export interface CheckoutErrorProps {
  heading: string;
  message?: string;
  onAction?: () => void;
  actionMessage?: string;
}

export const CheckoutError = ({
  heading,
  message,
  onAction,
  actionMessage = 'Try again',
}: CheckoutErrorProps) => {
  return (
    <Stack
      height="100%"
      divider={<Divider flexItem />}
    >
      <Container sx={{ height: '100%' }}>
        <Stack
          justifyContent="center"
          alignItems="center"
          spacing={4.5}
          height="100%"
        >
          <Xmark size={120} />
          <Typography variant="h2" color="secondary.main">{heading}</Typography>
          {message && <Typography variant="body2" color="text.secondary">{message}</Typography>}
        </Stack>
      </Container>
      {onAction && <Container sx={{ marginY: 2 }}>
        <Button fullWidth variant="contained" onClick={onAction}>{actionMessage}</Button>
      </Container>}
    </Stack>
  );
};