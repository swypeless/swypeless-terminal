import React, { useCallback, useEffect, useState } from 'react';
import { Button, Skeleton, Stack } from '@mui/material';
import {
  PaymentRequest,
  PaymentRequestOptions,
  StripeError,
} from '@stripe/stripe-js';
import {
  PaymentRequestButtonElement,
  useStripe,
} from '@stripe/react-stripe-js';
import { Sale } from 'types';
import { CardElementControls } from './CardElementControls';
import { useCheckoutStore } from 'stores';
import { LoadingButton } from '@mui/lab';
import { SaleUtils } from 'utils/sale-utils';

const makePaymentRequestItemOptions = (sale: Sale): Pick<PaymentRequestOptions, 'total' | 'displayItems'> => ({
  total: {
    amount: sale.amount,
    label: `Pay ${sale.merchant.display_name || 'Merchant'}`,
  },
  displayItems: sale.amount !== sale.subtotal
    ? SaleUtils.getSaleLineItems(sale).map(({ amount, label }) => ({ amount: amount || 0, label }))
    : [],
});

export interface PaymentControlsProps {
  sale: Sale;
  ready: boolean;
}

export const PaymentControls = ({
  sale,
  ready,
}: PaymentControlsProps) => {
  const stripe = useStripe();
  const [paymentRequest, setPaymentRequest] = useState<PaymentRequest | null>(null);
  const [paymentRequestChecked, setPaymentRequestChecked] = useState(false);
  const [wantShowPaymentRequest, setWantShowPaymentRequest] = useState(true);
  const [processing, setProcessing] = useState(false);
  const {
    tipNeedsUpdate,
    setPaymentError,
    setSaleComplete,
  } = useCheckoutStore((state) => ({
    tipNeedsUpdate: state.computed.tipNeedsUpdate,
    setPaymentError: state.setPaymentError,
    setSaleComplete: state.setSaleComplete,
  }));
  const [prevTipNeedsUpdate, setPrevTipNeedsUpdate] = useState(tipNeedsUpdate);

  const processPaymentResult = useCallback((error?: StripeError) => {
    if (error) {
      setPaymentError(error.message || null);
    } else {
      setSaleComplete();
    }
  }, [setPaymentError, setSaleComplete]);

  useEffect(() => {
    if (stripe) {
      const pr = stripe.paymentRequest({
        country: sale.merchant.country,
        currency: sale.currency,
        requestPayerName: true,
        requestPayerEmail: true,
        requestPayerPhone: true,
        ...makePaymentRequestItemOptions(sale),
      });

      // Check the availability of the Payment Request API.
      pr.canMakePayment().then(result => {
        if (result) {
          setPaymentRequest(pr);
        }
        setPaymentRequestChecked(true);
      });

      pr.on('paymentmethod', async (event) => {
        setProcessing(true);
        const { error } = await stripe.confirmCardPayment(
          sale.stripe_client_secret || '',
          { payment_method: event.paymentMethod.id },
        );
        if (error) {
          event.complete('fail');
        } else {
          event.complete('success');
        }
        setProcessing(false);
        processPaymentResult(error);
      });
    }
  }, [stripe, sale, setPaymentRequest, setPaymentRequestChecked, processPaymentResult, setProcessing]);
    
  useEffect(() => {
    if (!tipNeedsUpdate && prevTipNeedsUpdate) {
      paymentRequest?.update(makePaymentRequestItemOptions(sale));
    }
    setPrevTipNeedsUpdate(tipNeedsUpdate);
  }, [tipNeedsUpdate, prevTipNeedsUpdate, paymentRequest, sale, setPrevTipNeedsUpdate]);

  const showCardElement = useCallback(
    () => setWantShowPaymentRequest(false),
    [setWantShowPaymentRequest],
  );

  const showPaymentRequest = useCallback(
    () => setWantShowPaymentRequest(true),
    [setWantShowPaymentRequest],
  );

  const canPaymentRequest = paymentRequestChecked && paymentRequest !== null;
  const showingPaymentRequest = canPaymentRequest && wantShowPaymentRequest;

  return (
    <Stack
      py={2}
      spacing={1}
    >
      {paymentRequestChecked
        ? <>
          {showingPaymentRequest && (ready
            ? <PaymentRequestButtonElement options={{ paymentRequest }} />
            : <LoadingButton variant="contained" loading={true}>Loading</LoadingButton>
          )}
          {!showingPaymentRequest && <CardElementControls
            clientSecret={sale.stripe_client_secret || ''}
            ready={ready}
            processing={processing}
            setProcessing={setProcessing}
            processPaymentResult={processPaymentResult}
          />}
          {canPaymentRequest && (showingPaymentRequest
            ? <Button onClick={showCardElement} sx={{ fontSize: '0.7rem' }} disabled={processing}>Enter Card Details</Button>
            : <Button onClick={showPaymentRequest} sx={{ fontSize: '0.7rem' }} disabled={processing}>Buy With Wallet</Button>
          )}
        </>
        : <Skeleton variant="rectangular" animation="wave" height="40px" sx={{ borderRadius: 1 }} />}
    </Stack>
  );
};