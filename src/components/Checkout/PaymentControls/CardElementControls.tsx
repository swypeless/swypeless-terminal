import React, { useCallback, useState } from 'react';
import { CardElement, useElements, useStripe } from '@stripe/react-stripe-js';
import {
  StripeCardElementChangeEvent,
  StripeCardElementOptions,
  StripeError,
} from '@stripe/stripe-js';
import Button from '@mui/material/Button';
import CircularProgress from '@mui/material/CircularProgress';
import { theme } from 'styles/theme';
import Box from '@mui/material/Box';



export interface CardElementControlsProps {
  disabled?: boolean
  clientSecret: string;
  ready: boolean;
  processing: boolean;
  setProcessing: (processing: boolean) => void;
  processPaymentResult?: (error?: StripeError) => void;
}

export const CardElementControls = ({
  disabled,
  clientSecret,
  ready,
  processing,
  setProcessing,
  processPaymentResult,
}: CardElementControlsProps) => {
  const [buttonEnabled, setButtonEnabled] = useState(false);
  const stripe = useStripe();
  const elements = useElements();

  const cardElementChange = useCallback((event: StripeCardElementChangeEvent) => {
    setButtonEnabled(event.complete);
  }, [setButtonEnabled]);

  const handleClick = useCallback(async () => {
    if (!stripe || !elements) return;
    setProcessing(true);
    const { error } = await stripe.confirmCardPayment(clientSecret, {
      payment_method: {
        card: elements.getElement(CardElement)!,
      },
    });
    if (processPaymentResult) processPaymentResult(error);
    setProcessing(false);
  }, [clientSecret, setProcessing, stripe, elements, processPaymentResult]);

  const cardOptions: StripeCardElementOptions = {
    style: {
      base: {
        color: theme.palette.text.secondary,
        fontSize: `${theme.typography.fontSize}px`,
        backgroundColor: theme.palette.background.paper,
      },
      complete: {
        iconColor: theme.palette.primary.main,
        color: theme.palette.primary.main,
      },
      invalid: {
        iconColor: theme.palette.text.primary,
        color: theme.palette.text.primary,
      },
    },
  };

  return (
    <>
      <Box
        p={1.5}
        borderRadius={1}
        boxShadow="inset 0 1px 2px 1px rgba(0, 0, 0, 0.1)"
        bgcolor="background.paper"
      >
        <CardElement onChange={cardElementChange} options={cardOptions} />
      </Box>
      <Button
        variant="contained"
        disabled={disabled || !buttonEnabled || processing || !ready}
        onClick={handleClick}
      >
        {processing
          ? <>
            <CircularProgress
              size={16}
              color="inherit"
              sx={{ marginRight: 2 }}
            />
            Processing...
          </>
          : "Pay"}
      </Button>
    </>
  );
};
