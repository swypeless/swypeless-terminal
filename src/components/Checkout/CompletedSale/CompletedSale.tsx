import React, { useCallback, useState } from 'react';
import Container from '@mui/material/Container';
import Stack from '@mui/material/Stack';
import { Sale } from 'types';
import { SaleDetails } from '../SaleDetails/SaleDetails';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import EastRoundedIcon from '@mui/icons-material/EastRounded';
import { Checkmark } from '../../Checkmark/Checkmark';
import { ReceiptControls } from '../../ReceiptControls/ReceiptControls';
import { Divider } from '@mui/material';

export interface CompletedSaleProps {
  sale: Sale;
  sendReceipt?: (email: string) => any;
}

export const CompletedSale = ({
  sale,
  sendReceipt,
}: CompletedSaleProps) => {
  const [isShowingDetails, setIsShowingDetails] = useState(false);

  const showDetails = useCallback(() => {
    setIsShowingDetails(true);
  }, [setIsShowingDetails]);
  const hideDetails = useCallback(() => {
    setIsShowingDetails(false);
  }, [setIsShowingDetails]);

  return (
    <>
      {isShowingDetails
        ? <SaleDetails onBackClick={hideDetails} sale={sale} />
        : <Stack
            height="100%"
            divider={<Divider flexItem />}
          >
            <Container sx={{ height: '100%', overflowY: 'auto' }}>
              <Stack
                justifyContent="center"
                alignItems="center"
                spacing={5}
                height="100%"
                sx={{ overflowY: "auto" }}
              >
                <Checkmark size={120} />
                <Typography variant="h2" color="secondary.main">Payment Complete</Typography>
                <Button onClick={showDetails} sx={{ color: 'text.secondary', textTransform: 'none' }}>
                  View Details
                  <EastRoundedIcon fontSize="small" sx={{ marginLeft: 1 }} />
                </Button>
              </Stack>
            </Container>
          {sendReceipt && <Container sx={{ marginY: 2 }}>
            <ReceiptControls sendReceipt={sendReceipt} />
          </Container>}
          </Stack>}
    </>
  );
}