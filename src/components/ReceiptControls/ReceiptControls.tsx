import { LoadingButton } from '@mui/lab';
import { Stack, TextField } from '@mui/material';
import React, { BaseSyntheticEvent, useCallback, useState } from 'react';
import { isEmailValid } from '../../utils/validation';

export interface ReceiptControlsProps {
  sendReceipt: (email: string) => Promise<boolean>;
}

export const ReceiptControls = ({
  sendReceipt,
}: ReceiptControlsProps) => {
  const [emailValid, setEmailValid] = useState(false);
  const [email, setEmail] = useState('');
  const [processing, setProcessing] = useState(false);
  const [sent, setSent] = useState(false);

  const handleChange = useCallback((event: BaseSyntheticEvent) => {
    const value = event.target.value;
    setEmail(value);
    setEmailValid(isEmailValid(value));
    setSent(false);
  }, [setEmail, setEmailValid, setSent]);

  const handleClick = useCallback(async () => {
    if (sent) return;
    setProcessing(true);
    const success = await sendReceipt(email);
    setSent(success);
    setProcessing(false);
  }, [email, sent, sendReceipt, setProcessing, setSent]);

  const buttonText = sent
    ? 'Receipt Sent!'
    : processing
      ? 'Sending'
      : 'Send Receipt';

  return (
    <Stack
      justifyContent="center"
      alignItems="center"
      spacing={1}
      sx={{ width: '100%' }}
    >
      <TextField
        fullWidth
        onChange={handleChange}
        size='small'
        placeholder='Receipt Email'
        disabled={processing}
        inputProps={{ inputMode: 'email' }} />
      <LoadingButton
        loading={processing}
        fullWidth
        onClick={handleClick}
        variant="contained"
        disabled={!emailValid}
      >{buttonText}</LoadingButton>
    </Stack>
  );
};