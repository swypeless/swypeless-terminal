import React, {
  KeyboardEvent,
  MutableRefObject,
  useCallback,
  useRef,
} from 'react';
import TextField, { TextFieldProps } from '@mui/material/TextField';

const decimalKeys = [".", ","];
const numericKeys = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];
const editKeys = ["Backspace", "Delete"];
const allowedKeys = [...decimalKeys, ...numericKeys, ...editKeys];

export type NumericTextFieldProps = TextFieldProps & {
  maxIntegerDigits?: number;
  maxFractionDigits?: number;
  parseValue?: (data: any) => number | null;
  onChangeParsed?: (value: number | null) => void;
  inputRef?: MutableRefObject<unknown>;
};

export const NumericTextField = ({
  maxIntegerDigits = 1,
  maxFractionDigits = 0,
  onSelect,
  onKeyPress,
  inputRef,
  inputProps,
  ...otherProps
}: NumericTextFieldProps) => {
  const _inputRef = useRef();
  const ownInputRef = inputRef || _inputRef;

  const getSelectionStart = useCallback(() => {
    return ((ownInputRef.current as any).selectionStart || null) as number | null;
  }, [ownInputRef]);

  const handleKeyPress = useCallback((event: KeyboardEvent<HTMLInputElement>) => {
    const stop = () => {
      event.preventDefault();
      event.stopPropagation();
    };
    // ensure key is allowed
    if (!allowedKeys.includes(event.key)) stop();
    const value = (event.target as any).value as string;
    const location = getSelectionStart();
    const decimalKey =
      decimalKeys.find((key) => value.indexOf(key) >= 0) || decimalKeys[0];
    const decimalLocation = value.indexOf(decimalKey);
    const [integerPart, fractionPart] = value.split(decimalKey);
    const isCursorInIntegerPart =
      location === null || location <= integerPart.length;
    // check if decimal character already exists
    // or if fraction digits are allowed
    if (
      decimalKeys.includes(event.key) &&
      (decimalLocation >= 0 || maxFractionDigits === 0)
    )
      stop();
    // on numeric input
    if (numericKeys.includes(event.key)) {
      // check if trying to add a new integer digit
      if (isCursorInIntegerPart && integerPart.length >= maxIntegerDigits)
        stop();
      // check if tryint to add new fraction digit
      if (
        !isCursorInIntegerPart &&
        (fractionPart || "").length >= maxFractionDigits
      )
        stop();
    }
      // maybe call passed keypress event
    if (onKeyPress) onKeyPress(event);
  }, [onKeyPress, maxFractionDigits, maxIntegerDigits, getSelectionStart]);

  return <TextField
    onKeyPress={handleKeyPress}
    inputRef={ownInputRef}
    inputProps={{ inputMode: 'decimal', ...inputProps }}
    {...otherProps}
    type="text"
  />;
};
