import React from 'react';
import Lottie, { LottieProps } from 'react-lottie';
import animationData from 'assets/lotties/xmark.json';


export interface XmarkProps extends Omit<LottieProps, 'options'> {
  size?: number | string;
}

export const Xmark = ({
  size,
  width,
  height,
  ...otherProps
}: XmarkProps) => {
  const options = {
    loop: false,
    autoplay: true,
    animationData,
    rendererSettings: {
      preserveAspectRatio: 'xMidYMid slice'
    },
  };

  return (
    <Lottie
      width={width || size}
      height={height || size}
      options={options}
      {...otherProps}
    />
  );
};
