import React from 'react';
import Lottie, { LottieProps } from 'react-lottie';
import animationData from 'assets/lotties/checkmark.json';


export interface CheckmarkProps extends Omit<LottieProps, 'options'> {
  size?: number | string;
}

export const Checkmark = ({
  size,
  width,
  height,
  ...otherProps
}: CheckmarkProps) => {
  const options = {
    loop: true,
    autoplay: true,
    animationData,
    rendererSettings: {
      preserveAspectRatio: 'xMidYMid slice'
    },
  };

  return (
    <Lottie
      width={width || size}
      height={height || size}
      options={options}
      {...otherProps}
    />
  );
};
