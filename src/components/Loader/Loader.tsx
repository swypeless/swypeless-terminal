import React from 'react';
import { Box } from '@mui/system';
import Lottie, { LottieProps } from 'react-lottie';
import blueLoader from 'assets/lotties/loader__klein-blue.json';
import lightGreyLoader from 'assets/lotties/loader__light-grey.json';

export interface LoaderProps extends Omit<LottieProps, 'options'> {
  size?: number | string;
  color?: 'blue' | 'light-grey';
}

export const Loader = ({
  size,
  color = 'blue',
  width,
  height,
  ...otherProps
}: LoaderProps) => {
  const animationData = {
    'blue': blueLoader,
    'light-grey': lightGreyLoader,
  }[color];

  const options = {
    loop: true,
    autoplay: true,
    animationData,
    rendererSettings: {
      preserveAspectRatio: 'xMidYMid slice'
    },
  };

  return (
    <Box
      height="100%"
      width="100%"
      display="flex"
      alignItems="center"
      justifyContent="center"
    >
      <Lottie
        width={width || size}
        height={height || size}
        options={options}
        {...otherProps}
      />
    </Box>
  );
};
