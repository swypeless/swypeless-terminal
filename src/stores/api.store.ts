import create from 'zustand';
import {
  apiClient,
  ApiException,
  ConnectionApiException,
  ServerFailuerApiException,
} from 'api';
import { AxiosRequestConfig } from 'axios';

export enum ApiConnectionError {
  Server = 'server',
  Network = 'network',
}

export interface ApiStore {
  // state
  activeRequests: number;
  connectionError: ApiConnectionError | null;
  // actions
  incrementActiveRequests: () => void;
  decrementActiveRequests: () => void;
  setConnectionError: (connectionError: ApiConnectionError | null) => void;
  // computed state
  computed: {
    requesting: boolean;
  };
}

export const useApiStore = create<ApiStore>((set, get) => ({
  activeRequests: 0,
  connectionError: null,
  /**
   * Increments the number of active requests
   */
  incrementActiveRequests: () => { set((state) => ({ activeRequests: state.activeRequests + 1 })) },
  /**
   * Decrements the number of active requests
   */
  decrementActiveRequests: () => {
    set((state) => ({
      activeRequests: state.activeRequests > 0
        ? state.activeRequests - 1
        : 0,
    }));
  },
  /**
   * Sets the connection error
   */
  setConnectionError: (connectionError: ApiConnectionError | null) => {
    set({ connectionError });
  },
  /**
   * Computed state
   */
  computed: {
    /**
     * Whether the api is actively making a request to the backend
     */
    get requesting() {
      return get().activeRequests > 0;
    },
  },
}));

// register api interceptors
apiClient.registerRequestInterceptor(() => useApiStore.getState().incrementActiveRequests());
apiClient.registerResponseInterceptor(() => {
  // set no connection error
  useApiStore.getState().setConnectionError(null);
  // decrement active requests
  useApiStore.getState().decrementActiveRequests();
});
apiClient.registerResponseExceptionInterceptor((
  request: AxiosRequestConfig,
  exception: ApiException,
  willRetry: boolean,
) => {
  // check for connection error
  switch (exception.constructor) {
    case ServerFailuerApiException:
      useApiStore.getState().setConnectionError(ApiConnectionError.Server);
      break;
    case ConnectionApiException:
      useApiStore.getState().setConnectionError(ApiConnectionError.Network);
      break;
    default:
      useApiStore.getState().setConnectionError(null);
      break;
  }
  // decrement active requests if not retrying
  if (!willRetry) useApiStore.getState().decrementActiveRequests();
});
