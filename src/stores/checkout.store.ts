import {
  addIdPrefix,
  Sale,
  SaleComponentType,
  SaleStatus,
  SALE_PREFIX,
  TipSaleComponent,
} from 'types';
import create from 'zustand';
import { apiClient } from '../api';

export interface CheckoutStore {
  // state
  loaded: boolean;
  sale: Sale | null;
  paymentError: string | null;
  originalAmount: number;
  amountNoTip: number;
  tipAmount: number | null;
  originalTipAmount: number | null;
  // actions
  loadSale: (saleId: string) => Promise<void>;
  setSale: (sale: Sale) => void;
  setTipAmount: (amount: number | null) => void;
  maybeUpdateSaleTip: () => Promise<boolean>;
  sendReceipt: (email: string) => Promise<boolean>;
  setPaymentError: (error: string | null) => void;
  setSaleComplete: () => void;
  // computed
  computed: {
    tipNeedsUpdate: boolean;
  };
}

export const useCheckoutStore = create<CheckoutStore>((set, get) => ({
  loaded: false,
  sale: null,
  paymentError: null,
  originalAmount: 0,
  amountNoTip: 0,
  tipAmount: null,
  originalTipAmount: 0,
  /**
   * Loads a sale from the backend
   * 
   * @param {string} saleId 
   */
  loadSale: async (saleId: string) => {
    set({ sale: null, loaded: false });
    try {
      const sale = await apiClient.sales.retrieve(addIdPrefix(saleId, SALE_PREFIX));
      get().setSale(sale);
    }
    catch (error) {
      // TODO maybe handle different error types
    }
    set({ loaded: true })
  },
  /**
   * Sets a sale and dependent properties in the store
   * 
   * @param {Sale} sale
   */
  setSale: (sale: Sale) => {
    let tipAmount = null;
    let originalTipAmount = null;
    let amountNoTip = sale.amount;
    const tipComponent = sale.components
      .find((component) => component.type === SaleComponentType.Tip) as TipSaleComponent | undefined;
    if (tipComponent) {
      tipAmount = tipComponent.amount;
      originalTipAmount = tipComponent.amount;
      amountNoTip = amountNoTip - (tipAmount || 0);
    }
    set({
      sale,
      originalAmount: sale.amount,
      amountNoTip,
      tipAmount,
      originalTipAmount,
    });
  },
  /**
   * Sets the tip amount of the sale
   * 
   * @param {number|null} amount 
   * @returns {Promise<void>}
   */
  setTipAmount: (amount: number | null) => {
    const { sale, amountNoTip } = get();
    if (!sale) return;
    const tipComponent = sale.components
      .find((component) => component.type === SaleComponentType.Tip) as TipSaleComponent | undefined;
    if (!tipComponent) return;
    tipComponent.amount = amount;
    sale.amount = amountNoTip + (amount || 0);
    set({
      tipAmount: amount,
      sale,
    });
  },
  /**
   * Tries to update the sale tip on the backend
   * 
   * @returns {Promise<boolean>}
   */
  maybeUpdateSaleTip: async () => {
    if (get().computed.tipNeedsUpdate) {
      try {
        const { sale, tipAmount } = get();
        if (!sale) return false;
        const updatedSale = await apiClient.sales.updateTip(sale.id, { amount: tipAmount });
        get().setSale(updatedSale);
      } catch (error) {
        // TODO handle error
        return false;
      }
    }
    return true;
  },
  sendReceipt: async (email: string) => {
    try {
      const { sale } = get();
      if (!sale) return false;
      await apiClient.sales.updateReceiptEmail(sale.id, { receipt_email: email });
    } catch (error) {
      // TODO handle error
      return false;
    }
    return true;
  },
  /**
   * Sets the payment error string
   * 
   * @param {string|null} error
   */
  setPaymentError: (error: string | null) => {
    set({ paymentError: error });
  },
  /**
   * Sets a sale as complete
   */
  setSaleComplete: () => {
    const { sale } = get();
    if (sale) {
      sale.status = SaleStatus.Completed;
      set({ sale });
    }
  },
  /**
   * Computed state
   */
  computed: {
    /**
     * Returns whether the tip needs to be updated on the backend
     */
    get tipNeedsUpdate() {
      const { tipAmount, originalTipAmount } = get();
      return tipAmount !== originalTipAmount;
    }
  }
}));
