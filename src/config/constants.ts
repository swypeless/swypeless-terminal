export const API_BASE_URL = process.env.REACT_APP_API_BASE_URL || '';
export const API_MAX_RETRIES = Number.parseInt(process.env.REACT_APP_API_MAX_RETRIES || '') || undefined;
export const API_TIMEOUT = Number.parseInt(process.env.REACT_APP_API_TIMEOUT || '') || undefined;
export const API_TOKEN_HEADER = process.env.REACT_APP_API_TOKEN_HEADER || 'authorization-token';

export const STRIPE_API_VERSION = process.env.REACT_APP_STRIPE_API_VERSION || '2020-08-27';
export const STRIPE_PUBLISHABLE_KEY = process.env.REACT_APP_STRIPE_PUBLISHABLE_KEY || '';
export const GA_MEASUREMENT_ID = process.env.REACT_APP_GA_MEASUREMENT_ID || '';

export const CURRENCY_MAX_DIGITS = 8;
