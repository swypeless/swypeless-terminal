import { useRef, useState, useCallback, useEffect } from 'react';


export interface FitDimensionProps {
  initial: number;
  min: number;
  max?: number;
  resolution?: number;
}

export interface FitDimensionState {
  dimension: number;
  dimensionPrev?: number;
  dimensionMin: number;
  dimensionMax: number;
}

export const useFitDimension = (props: FitDimensionProps) => {
  const {
    initial,
    min,
    max = props.initial,
    resolution = 1,
  } = props;

  const ref = useRef<HTMLSpanElement>(null);

  const [state, setState] = useState<FitDimensionState>({
    dimension: initial,
    dimensionMin: min,
    dimensionMax: max,
  });
  const {
    dimension,
    dimensionPrev,
    dimensionMin,
    dimensionMax,
  } = state;

  const triggerReset = useCallback(() => {
    setState({
      dimension: initial,
      dimensionPrev: undefined,
      dimensionMin: min,
      dimensionMax: max,
    });
  }, [setState, initial, min, max]);

  useEffect(() => {
    const isOverflow =
      !!ref.current && ref.current.scrollWidth > ref.current.offsetWidth;
    const isDone = (dimensionPrev === undefined && !isOverflow && dimension >= dimensionMax)
      || (dimensionPrev && Math.abs(dimension - dimensionPrev) <= resolution);
    const isAsc = dimensionPrev && dimension > dimensionPrev;

    // return if the dimension has been adjusted "enough" (change within resolution)
    // reduce font dimension by one increment if it's overflowing
    if (isDone) {
      if (isOverflow) {
        const dimensionNew =
          dimensionPrev
            ? dimensionPrev < dimension
              ? dimensionPrev
              : dimension - (dimensionPrev - dimension)
            : dimension;
        setState({
          ...state,
          dimension: dimensionNew,
          dimensionMin,
          dimensionMax,
          dimensionPrev,
        });
      }
      return;
    }

    // binary search to adjust font size
    let delta;
    let newMax = dimensionMax;
    let newMin = dimensionMin;
    if (isOverflow) {
      delta = isAsc ? dimensionPrev - dimension : dimensionMin - dimension;
      newMax = Math.min(dimensionMax, dimension);
    } else {
      delta = isAsc ? dimensionMax - dimension : (dimensionPrev || dimensionMin) - dimension;
      newMin = Math.max(dimensionMin, dimension);
    }
    setState({
      ...state,
      dimension: dimension + delta / 2,
      dimensionMax: newMax,
      dimensionMin: newMin,
      dimensionPrev: dimension,
    });
  }, [dimension, dimensionMin, dimensionMax, dimensionPrev, resolution, ref, state, setState]);

  return { dimension, ref, triggerReset };
};