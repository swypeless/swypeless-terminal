import ReactGA from 'react-ga4';
import { GA_MEASUREMENT_ID } from '../config/constants';

export default class Analytics {
  static initialize() {
    ReactGA.initialize(GA_MEASUREMENT_ID);
  }

  static trackPage(page: string) {
    ReactGA.set({ page });
    ReactGA.send('pageview');
  }
}