const utilityMath = {
  /**
   * Calculates the pips value of amount (integer)
   *
   * 1 pip = 0.0001 = 0.01%
   *
   * @param {number} amount
   * @param {number} pips
   * @returns {number} integer
   */
  pipsOfAmountInteger(amount: number, pips: number): number {
    return Math.round(this.pipsOfAmount(amount, pips));
  },
  /**
   * Calculates the pips value of amount
   *
   * 1 pip = 0.0001 = 0.01%
   *
   * @param {number} amount
   * @param {number} pips
   * @returns {number}
   */
  pipsOfAmount(amount: number, pips: number): number {
    return amount * this.pipsToDecimal(pips);
  },
  /**
   * Converts pips to decimal
   *
   * 1 pip = 0.0001 = 0.01%
   *
   * @param {number} pips
   * @returns {number}
   */
  pipsToDecimal(pips: number): number {
    return this.percentToDecimal(this.pipsToPercent(pips));
  },
  /**
   * Converts decimal to pips
   *
   * 1 pip = 0.0001 = 0.01%
   *
   * @param {number} decimal
   * @returns {number}
   */
  decimalToPips(decimal: number): number {
    return this.percentToPips(this.decimalToPercent(decimal));
  },
  /**
   * Converts pips to percent
   *
   * 1 pip = 0.0001 = 0.01%
   *
   * @param {number} pips
   * @returns {number}
   */
  pipsToPercent(pips: number): number {
    return pips / 100;
  },
  /**
   * Converts percent to pips
   *
   * 1 pip = 0.0001 = 0.01%
   *
   * @param {number} percent
   * @returns {number}
   */
  percentToPips(percent: number): number {
    return percent * 100;
  },
  /**
   * Converts percent to decimal
   *
   * 1 pip = 0.0001 = 0.01%
   *
   * @param {number} percent
   * @returns {number}
   */
  percentToDecimal(percent: number): number {
    return percent / 100;
  },
  /**
   * Converts decimal to percent
   *
   * 1 pip = 0.0001 = 0.01%
   *
   * @param {number} percent
   * @returns {number}
   */
  decimalToPercent(decimal: number): number {
    return decimal * 100;
  },
  /**
   * Converts mcus to decimal based on the number of fraction digits
   * 
   * @param {number} mcus 
   * @param {number} fractionDigits 
   * @returns {number}
   */
  mcusToDecimal(mcus: number, fractionDigits: number): number {
    return mcus / 10 ** fractionDigits;
  },
  /**
   * Converts decimal to mcus based on the number of fraction digits
   * 
   * @param {number} decimal 
   * @param {number} fractionDigits 
   * @returns {number}
   */
  decimalToMcus(decimal: number, fractionDigits: number): number {
    return Math.round(decimal * 10 ** fractionDigits);
  },
  /**
   * Rounds a value to the nearest increment
   *
   * @param {number} value
   * @param {number} increment
   * @returns {number}
   */
  roundToIncrement(value: number, increment: number): number {
    return Math.round(value / increment) * increment;
  },
  /**
   * 
   * @template T
   * @param {T[]} list
   * @param {(element: T) => number} [getValue] 
   * @returns {number}
   */
  sum<T>(list: T[], getValue?: (element: T) => number): number {
    return list.reduce(
      (prev, element) => prev
        + (getValue === undefined
          ? typeof element === 'number'
            ? element
            : 0
          : getValue(element)),
      0,
    );
  },
};
Object.freeze(utilityMath);

export const UtilityMath = utilityMath;
export default UtilityMath;
