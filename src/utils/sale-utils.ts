import {
  ConvenienceFeeSaleComponent,
  Sale,
  SaleComponentType,
  SaleLineItem,
  TaxSaleComponent,
  TipSaleComponent,
} from '../types';
import { PercentUtils } from './percent-utils';


const saleUtils = {
  /**
   * Returns the sale line items of a sale
   * 
   * @param {Sale} sale
   * @returns {SaleLineItem[]}
   */
  getSaleLineItems(sale: Sale): SaleLineItem[] {
    const lineItems: SaleLineItem[] = [];
  
    const taxComponent = sale.components
      .find((component) => component.type === SaleComponentType.Tax) as TaxSaleComponent | undefined;
    if (taxComponent) lineItems.push({
      label: `Sales Tax (${PercentUtils.formatPercentFromPips(taxComponent.rate)})`,
      amount: taxComponent.amount || 0,
      currency: taxComponent.currency,
    });
  
    const convenienceFeeComponent = sale.components
      .find((component) => component.type === SaleComponentType.ConvenienceFee) as ConvenienceFeeSaleComponent | undefined;
    if (convenienceFeeComponent) lineItems.push({
      label: 'Convenience Fee',
      amount: convenienceFeeComponent.amount || 0,
      currency: convenienceFeeComponent.currency,
    });
  
    const tipComponent = sale.components
      .find((component) => component.type === SaleComponentType.Tip) as TipSaleComponent | undefined;
    if (tipComponent && tipComponent.amount) lineItems.push({
      label: 'Tip',
      amount: tipComponent.amount,
      currency: tipComponent.currency,
    });
  
    if (lineItems.length > 0) {
      const count = sale.products.reduce((prev, product) => prev + product.quantity, 0);
      lineItems.unshift({
        label: `Subtotal (${count} item${count > 1 ? 's' : ''})`,
        amount: sale.subtotal,
        currency: sale.currency,
      });
    }
  
    return lineItems;
  },
}
Object.freeze(saleUtils);

export const SaleUtils = saleUtils;
