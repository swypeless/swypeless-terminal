import { UtilityMath } from './utility-math';

const currencyUtils = {
  /**
   * Return the number of fraction digits in a currency
   * 
   * @param {string} currency
   * @returns {number}
   */
  currencyFractionDigits(currency: string): number {
    const formatted = 1.23456.toLocaleString('en-US', {
      style: 'currency',
      currency,
    });
    const parts = formatted.split('.');
    return parts[1] ? parts[1].length : 0;
  },
  /**
   * Returns the symbol of the currency
   * 
   * @param {string} currency
   * @param {string} [locale] 
   * @returns {string|null}
   */
  currencySymbol(currency: string, locale?: string): string | null {
    const symbol = new Intl.NumberFormat(locale, { style: 'currency', currency })
      .formatToParts(1).find(x => x.type === 'currency');
    return (symbol && symbol.value) || null;
  },
  /**
   * Returns the decimal character of a currency
   * 
   * @param {string} currency
   * @param {string} [locale]
   * @returns {string|null}
   */
  currencyDecimal(currency: string, locale?: string): string | null {
    const decimal = new Intl.NumberFormat(locale, { style: 'currency', currency })
      .formatToParts(1).find(x => x.type === 'decimal');
    return (decimal && decimal.value) || null;
  },
  /**
   * Parses a value into the mcus of a given currency
   * 
   * @param {any} value
   * @param {string} currency
   * @returns {number|null}
   */
  parseCurrencyAsMcus(value: any, currency: string): number | null {
    // replace decimal and parse number
    const decimalValue = Number.parseFloat(`${value}`.replace(",", "."));
    // verify a valid number
    if (Number.isFinite(decimalValue)) {
      // convert to mcus (integer)
      const fractionDigits = this.currencyFractionDigits(currency);
      return Math.round(decimalValue * 10 ** fractionDigits);
    }
    return null;
  },
  /**
   * Formats mcus into a currency string based on the currency and locale
   * 
   * @param {number} mcus 
   * @param {string} currency 
   * @param {string} locale 
   * @returns {string}
   */
  formatCurrency(mcus: number, currency: string, locale?: string): string {
    const fractionDigits = this.currencyFractionDigits(currency);
    return UtilityMath.mcusToDecimal(
      mcus,
      fractionDigits,
    ).toLocaleString(locale, {
      style: 'currency',
      currency,
      minimumIntegerDigits: 1,
    });
  },
};
Object.freeze(currencyUtils);

export const CurrencyUtils = currencyUtils;
