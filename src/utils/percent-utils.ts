import { UtilityMath } from './utility-math';

const percentUtils = {
  /**
   * Formats a percent based on the locale and other number format options
   * 
   * @param {number} value 
   * @param {string} locale 
   * @param {Intl.NumberFormatOptions} options 
   * @returns {string}
   */
  formatPercent(value: number, locale?: string, options?: Intl.NumberFormatOptions): string {
    return value.toLocaleString(locale, {
      minimumFractionDigits: 1,
      maximumFractionDigits: 2,
      minimumSignificantDigits: 2,
      minimumIntegerDigits: 1,
      ...(options || {}),
      style: 'percent',
    });
  },
  /**
   * Formats a decimal number as a percent based on the locale and other number format options
   * 
   * @param {number} decimal 
   * @param {string} locale 
   * @param {Intl.NumberFormatOptions} options 
   * @returns {string}
   */
  formatPercentFromDecimal(decimal: number, locale?: string, options?: Intl.NumberFormatOptions): string {
    return this.formatPercent(decimal, locale, options);
  },
  /**
   * Formats a percent based on the locale and other number format options
   * 
   * @param {number} percent 
   * @param {string} locale 
   * @param {Intl.NumberFormatOptions} options 
   * @returns {string}
   */
  formatPercentFromPercent(percent: number, locale?: string, options?: Intl.NumberFormatOptions): string {
    return this.formatPercent(
      UtilityMath.percentToDecimal(percent),
      locale,
      options,
    );
  },
  /**
   * Formats a pips value as a percent based on the locale and other number format options
   * 
   * @param {number} pips 
   * @param {string} locale 
   * @param {Intl.NumberFormatOptions} options 
   * @returns {string}
   */
  formatPercentFromPips(pips: number, locale?: string, options?: Intl.NumberFormatOptions): string {
    return this.formatPercent(
      UtilityMath.pipsToDecimal(pips),
      locale,
      options,
    );
  },
};
Object.freeze(percentUtils);

export const PercentUtils = percentUtils;
