import { createTheme } from '@mui/material';

declare module '@mui/material/styles' {
  interface Palette {
    selected: Palette['primary'];
    dividerSecondary?: string;
  }
  interface PaletteOptions {
    selected?: PaletteOptions['primary'];
    dividerSecondary?: string;
  }
}

export const theme = createTheme({
  palette: {
    primary: {
      main: '#252dab',
    },
    secondary: {
      main: '#191b68',
    },
    background: {
      default: '#ffffff',
      paper: '#fafcfe',
    },
    selected: {
      main: '#797dab',
    },
    success: {
      main: '#72a98f',
    },
    error: {
      main: '#ef233c',
    },
    warning: {
      main: '#ffba49',
    },
    text: {
      primary: '#312e5e',
      secondary: '#6f6f80',
      disabled: '#e0e0e0',
    },
    divider: '#e8e8f0',
    dividerSecondary: '#f5f5f5',
  },
  typography: {
    fontSize: 16,
    h1: {
      fontSize: '5rem',
      letterSpacing: '-0.05em',
      fontWeight: 300,
      lineHeight: 1,
    },
    h2: {
      fontSize: '2rem',
      letterSpacing: '-0.02em',
      lineHeight: 1,
    },
    h3: {
      fontSize: '1.5rem',
      letterSpacing: '-0.02em',
      lineHeight: 1,
      fontWeight: 400,
    },
    h4: {
      fontSize: '1.25rem',
      lineHeight: 1,
      letterSpacing: '-0.02em',
    },
    subtitle1: {
      fontSize: '1.25rem',
      letterSpacing: '0em',
    },
    body1: {
      fontSize: '1rem',
      lineHeight: 1.4,
      letterSpacing: '0em',
    },
    body2: {
      fontSize: '1rem',
      lineHeight: 1.4,
    },
    button: {
      fontSize: '0.9rem',
      letterSpacing: '0.05em',
      fontWeight: 500,
    },
  },
  shape: {
    borderRadius: 8,
  },
});